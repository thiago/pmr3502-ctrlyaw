import numpy as np
import motorctrl
import contextlib

# Cria um controlador de orientação.
#   Este gerador é decorado com contextmanager para garantir
#   que o estado do gpio e pwm seja resetado na saída de escopo
@contextlib.contextmanager
def create_yaw_controller(t0, setpoint):
    # Configuração inicial: output com nível inicial baixo
    print("Criando controle de orientação...", end="")
    with motorctrl.cria_controle_motor() as ctrl:
        yawctrl = Controller(t0, setpoint, 0, ctrl)
        try:
            yield yawctrl
        finally:
            yawctrl.stop()
    print("Feito.")


class Controller():
    def __init__(self, t0, setpoint, surge, motorctrl):
        # Controlador do motor
        self._ctrl = motorctrl
        # Setpoint de rumo
        self._setpoint = setpoint
        # Ação de avanço
        self._surge = surge
        # Valor do tempo na última atualização
        self._t = t0
        # Valor do acumulador de integração na última atualização
        self._I = 0

    # Novo setpoint de rumo
    def newyawsetpoint(self, new):
        self._setpoint = new

    # Nova ação de avanço
    def newsurge(self, new):
        self._surge = new

    # Encerra o controlador
    def stop(self):
      print("Parando Controlador")
      self._ctrl.set_lr(0, 0)
      print("Controlador Parado")

    # Nova estimativa
    #   t é o momento da estimativa
    #   theta é a estimativa de rumo (em radianos)
    #   sigma é a covariância da estimativa de rumo
    #   omega é a estimativa da velocidade angular
    def newestimate(self, t, theta, sigma, omega):
        if sigma>0.1:
            return
        # Complete com seu código
        pass



