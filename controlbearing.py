#!/usr/bin/env python3

import selectors

import signal
import asyncio
import getopt
import sys
import subprocess
import time
import functools
import signal
import os
import controller
import rumo


class ImuClient():
    def __init__(self, address, port, datacallback):
        self._server = address
        self._port = port
        self._datacallback = datacallback
        self._keepRunning = False
        self._read = None

    def start(self):
        pass

    def stop(self):
        print("Encerrando...")
        self._keepRunning = False
        if self._writer:
            self._writer.close()
            self._reader = None
            self._writer = None

    def started(self):
        return self._keepRunning

    def __del__(self):
        pass

    def _sighandler(self):
        print('Signal Received')
        self._keepRunning = False
        self.stop()

    async def _mainloop(self):
        loop = asyncio.get_running_loop()

        for signame in {'SIGINT', 'SIGTERM'}:
            loop.add_signal_handler(
                getattr(signal, signame),
                self._sighandler)
        print("Openining connection... ", end="")
        try:
            self._reader, self._writer = await asyncio.open_connection(self._server, self._port)
        except OSError as e:
            print("Failed")
            self._keepRunning = False
        else:
            print("Done,")
        while self._keepRunning:
            data = await self._reader.read(32)
            if len(data)==32:
                self._datacallback(data)
            else:
                self.stop()

    def run(self):
        self._keepRunning = True
        #print("run")
        asyncio.run(self._mainloop())



def main():

    with controller.create_yaw_controller(time.clock_gettime(time.CLOCK_REALTIME), 0) as cc:
        kf = rumo.cria_estimador_rumo(time.clock_gettime(time.CLOCK_REALTIME), 0, 40, cc.newestimate, "kf")

        imuclient = ImuClient("localhost", 1234, kf.callback)
        imuclient.run()

    return 0

if __name__=="__main__":
    sys.exit(main())

